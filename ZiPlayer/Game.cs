﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using static IntuitiveRNG.IntuitiveRNG;
using AnyBitVars;
using TernaryLogic;
using ZiPlayer.lib;
using static ZiPlayer.lib.GameVars;
using static ZiPlayer.lib.Methods;
using static ZiPlayer.lib.Initializers;
using System.IO;
using NAudio.Wave;

namespace ZiPlayer
{
    /// <summary>This is the main type for your game.</summary>
    public class CurrentGame : Game
    {
        string fullTitle = "";
        int i = 0;
        WaveOutEvent waveOut = new WaveOutEvent();
        MediaFoundationReader reader;
        ListP<string> playlist = new ListP<string>();
        Texture2D pauseButtonTexture, playButtonTexture;

        public CurrentGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        void UpdateTitle()
        {
            TimeSpan temp = new TimeSpan(0, 0, 0, (int)timers[1].timeSinceTick, (int)Math.Round((timers[1].timeSinceTick - (int)timers[1].timeSinceTick) * 10));
            lastTimeSpan = temp;
            string tempTitle = fullTitle;
            if (fullTitle.Length >= 14)
            {
                if (i >= fullTitle.Length)
                {
                    i = 0;
                }
                if (i + 14 > fullTitle.Length)
                {
                    tempTitle = (fullTitle + ' ').Substring(i) + (fullTitle + ' ').Substring(0, i + 14 - (fullTitle + ' ').Length);
                }
                else
                {
                    tempTitle = fullTitle.Substring(i, 14);
                }
                i++;
            }
            Window.Title = (fullTitle.Length > 62 ? fullTitle.Substring(0, 59) + "..." : fullTitle) + " - " + temp.Minutes.ToString() + ':' + temp.Seconds.ToString().PadLeft(2, '0');
            menus[0].copy[0].message = tempTitle;
            menus[0].copy[1].message = temp.Minutes.ToString() + ':' + temp.Seconds.ToString().PadLeft(2, '0');
        }

        void UpdateVolume()
        {
            Properties.Settings.Default.Volume = 1 - ((mouseLocation.Y - menus[0].buttons[1].bounds.Y) / menus[0].buttons[1].bounds.Height);
            waveOut.Volume = Properties.Settings.Default.Volume;
            menus[0].buttons[3].bounds.Y = mousePos.Y - 3;
        }

        void UpdatePosition()
        {
            if (timers.Count > 0)
            {
                timers[1].timeSinceTick = reader.TotalTime.TotalSeconds * ((mouseLocation.X - menus[0].buttons[0].bounds.X) / menus[0].buttons[0].bounds.Width);
                reader.CurrentTime = TimeSpan.FromSeconds(timers[1].timeSinceTick);
                menus[0].buttons[2].bounds.X = mousePos.X - 3;
            }
        }

        void PreviousSong()
        {
            if (playlist.point <= 0)
            {
                playlist.point = 0;
                ResetSong();
            }
            else
            {
                playlist.point--;
                ResetSong();
            }
        }

        void NextSong()
        {
            playlist.point++;
            if (playlist.point >= playlist.Count)
            {
                waveOut.Stop();
                reader.Dispose();
                waveOut.Dispose();
                Exit();
            }
            else
            {
                Timer nextSongTimer = new Timer(reader.TotalTime.TotalSeconds);
                nextSongTimer.OnTimerTick += NextSong;
                timers.Add(nextSongTimer);
                ResetSong();
            }
        }

        void ResetSong()
        {
            waveOut.Stop();
            i = 0;
            fullTitle = Path.GetFileNameWithoutExtension(new FileInfo(playlist.GetValueAtPoint()).Name);
            reader = new MediaFoundationReader(playlist.GetValueAtPoint());
            waveOut.Init(reader);
            timers[1].timeSinceTick = 0;
            Play();
        }

        void PlayPause()
        {
            if (timers[1].active)
            {
                Pause();
            }
            else
            {
                Play();
            }
        }

        void Play()
        {
            waveOut.Stop();
            timers[1].active = true;
            waveOut.Play();
            menus[0].buttons[6].sprites[0] = pauseButtonTexture;
        }

        void Pause()
        {
            waveOut.Pause();
            timers[1].active = false;
            menus[0].buttons[6].sprites[0] = playButtonTexture;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            InitializeVariables();
            SetupSettingsAndGraphics();
            
            levels.OnPointChanged += ResetPlayerLocation;
#if DEBUG
            if (Program.args.Length == 0)
            {
                Program.args = new string[] { @"D:\Documents\Music\Music\Hate9\Contextual Mediocrity.mp3" };
            }
#endif
            if (Program.args.Length > 0)
            {

                try
                {
                    foreach (string arg in Program.args)
                    {
                        if ((File.GetAttributes(arg) & FileAttributes.Directory) == FileAttributes.Directory)
                        {//directory
                            List<string> allFiles = Directory.GetFiles(arg, "*.mp3", SearchOption.AllDirectories).ToList();
                            allFiles.AddRange(Directory.GetFiles(arg, "*.wav", SearchOption.AllDirectories).ToList());
                            allFiles.AddRange(Directory.GetFiles(arg, "*.ogg", SearchOption.AllDirectories).ToList());
                            allFiles.AddRange(Directory.GetFiles(arg, "*.wma", SearchOption.AllDirectories).ToList());
                            allFiles.AddRange(Directory.GetFiles(arg, "*.aac", SearchOption.AllDirectories).ToList());
                            allFiles.AddRange(Directory.GetFiles(arg, "*.m4a", SearchOption.AllDirectories).ToList());
                            allFiles.Sort();
                            foreach (string file in allFiles)
                            {
                                playlist.Add(file);
                            }
                        }
                        else
                        {//file
                            playlist.Add(arg);
                        }
                    }
                    playlist.Sort();

                    reader = new MediaFoundationReader(playlist.GetValueAtPoint());
                    waveOut.Init(reader);
                    waveOut.Volume = Properties.Settings.Default.Volume;
                    fullTitle = Path.GetFileNameWithoutExtension(new FileInfo(playlist.GetValueAtPoint()).Name);
                    Window.Title = (fullTitle.Length > 62 ? fullTitle.Substring(0, 59) + "..." : fullTitle) + " - 0:00";

                    Timer titleChangeTimer = new Timer(1, true);
                    titleChangeTimer.OnTimerTick += UpdateTitle;
                    timers.Add(titleChangeTimer);

                    Timer nextSongTimer = new Timer(reader.TotalTime.TotalSeconds);
                    nextSongTimer.OnTimerTick += NextSong;
                    timers.Add(nextSongTimer);

                    waveOut.Play();
                }
                catch
                {
                    Exit();
                }
            }
            else
            {
                Exit();
            }

            Button line = new Button(new Rectangle(12, 36, 223, 3), Content.Load<Texture2D>("line"));
            line.LeftDown += UpdatePosition;

            Button line2 = new Button(new Rectangle(4, 3, 3, 58), Content.Load<Texture2D>("line2"));
            line2.LeftDown += UpdateVolume;
            line2.LeftClick += Properties.Settings.Default.Save;

            Button marker = new Button(new Rectangle(4, 34, 6, 4), Content.Load<Texture2D>("marker"));
            
            Button marker2 = new Button(new Rectangle(3, (int)Math.Round((1 - Properties.Settings.Default.Volume) * 58) + line2.bounds.Y, 5, 5), Content.Load<Texture2D>("marker2"));

            Button previousButton = new Button(new Rectangle(20, 44, 16, 16), new Polygon(new Point(22, 51), new Point(27, 46), new Point(32, 46), new Point(32, 57), new Point(27, 57), new Point(22, 52)), Content.Load<Texture2D>("previousButton"));
            previousButton.LeftClick += PreviousSong;

            Button nextButton = new Button(new Rectangle(52, 44, 16, 16), new Polygon(new Point(55, 46), new Point(60, 46), new Point(65, 51), new Point(65, 52), new Point(60, 57), new Point(55, 57)), Content.Load<Texture2D>("nextButton"));
            nextButton.LeftClick += NextSong;

            playButtonTexture = Content.Load<Texture2D>("playButton");
            pauseButtonTexture = Content.Load<Texture2D>("pauseButton");
            Button playPauseButton = new Button(new Rectangle(36, 44, 16, 16), pauseButtonTexture);
            playPauseButton.LeftClick += PlayPause;

            List<Button> buttons = new List<Button>
            {
                line,
                line2,
                marker,
                marker2,
                previousButton,
                nextButton,
                playPauseButton
            };

            List<Text> text = new List<Text>();
            
            text.Add(new Text(new Vector2(12, -10), fullTitle.Length > 14 ? fullTitle.Substring(0, 14) : fullTitle, Content.Load<SpriteFont>("font")));
            text.Add(new Text(new Vector2(200, -10), "0:00", Content.Load<SpriteFont>("font")));

            Menu mainMenu = new Menu(Color.White, buttons, text);
            menus.Add(mainMenu);
            
            IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            preRenderTarget = new RenderTarget2D(GraphicsDevice, camera.Width, camera.Height);
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {

        }

        TimeSpan lastTimeSpan = new TimeSpan();

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            UpdateTimers(gameTime);

            if (IsActive)
            {
                UpdateInputs();
                UpdateButtons(menus.GetValueAtPoint().buttons);

                ProcessKeys();
            }

            if (timers.Count > 0)
            {
                if (timers.Count > 1 ? timers[1].active : false)
                {
                    menus[0].buttons[2].bounds.X = (int)Math.Round(timers[1].timeSinceTick / timers[1].totalTimeTilTick * 221) + menus[0].buttons[0].bounds.X - 3;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>This is called when the game should draw itself.</summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.SetRenderTarget(preRenderTarget);
            spriteBatch.Begin(samplerState: SamplerState.PointClamp);
            if (flags[0])
            {
                GraphicsDevice.Clear(Color.White);
                foreach (GameObject obj in levels.GetValueAtPoint().objects)
                {
                    spriteBatch.Draw(obj.sprites.GetValueAtPoint(), new Rectangle(obj.bounds.X - camera.X, obj.bounds.Y - camera.Y, obj.bounds.Width, obj.bounds.Height), null, Color.White, (float)obj.rotation, new Vector2(obj.centerPoint.X, obj.centerPoint.Y), SpriteEffects.None, 0f);
                }

                spriteBatch.Draw(character.sprites.GetValueAtPoint(), new Rectangle(character.bounds.X - camera.X, character.bounds.Y - camera.Y, character.bounds.Width, character.bounds.Height), null, Color.White, (float)character.rotation, new Vector2(character.centerPoint.X, character.centerPoint.Y), SpriteEffects.None, 0f);
                //interface
                foreach (GameObject obj in levelInterface.objects)
                {
                    spriteBatch.Draw(obj.sprites.GetValueAtPoint(), new Rectangle(obj.bounds.X - camera.X, obj.bounds.Y - camera.Y, obj.bounds.Width, obj.bounds.Height), null, Color.White, (float)obj.rotation, new Vector2(obj.centerPoint.X, obj.centerPoint.Y), SpriteEffects.None, 0f);
                }
                foreach (Button button in levelInterface.buttons)
                {
                    spriteBatch.Draw(button.sprites.GetValueAtPoint(), new Rectangle(button.bounds.X - camera.X, button.bounds.Y - camera.Y, button.bounds.Width, button.bounds.Height), Color.White);
                }
                foreach (Text text in levelInterface.copy)
                {
                    spriteBatch.DrawString(text.font, text.message, new Vector2(text.location.X - camera.X, text.location.Y - camera.Y), Color.Black);
                }
            }
            else
            {
                GraphicsDevice.Clear(menus.GetValueAtPoint().backgroundColor);
                foreach (Button button in menus.GetValueAtPoint().buttons)
                {
                    spriteBatch.Draw(button.sprites.GetValueAtPoint(), new Rectangle(button.bounds.X - camera.X, button.bounds.Y - camera.Y, button.bounds.Width, button.bounds.Height), Color.White);
                }
                foreach (Text text in menus.GetValueAtPoint().copy)
                {
                    spriteBatch.DrawString(text.font, text.message, new Vector2(text.location.X - camera.X, text.location.Y - camera.Y), Color.Black);
                }
            }
            if (!IsMouseVisible && showCursor && GraphicsDevice.Viewport.Bounds.Contains(mousePos))
            {
                spriteBatch.Draw(cursor.GetValueAtPoint(), new Vector2(mouseLocation.X - camera.X, mouseLocation.Y - camera.Y), Color.White);
            }
            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);

            spriteBatch.Begin(samplerState: SamplerState.PointClamp);
            spriteBatch.Draw(preRenderTarget, GraphicsDevice.Viewport.Bounds, new Rectangle(Point.Zero, camera.Size), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
