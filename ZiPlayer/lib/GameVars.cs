﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using static ZiPlayer.Program;

namespace ZiPlayer.lib
{
    static class GameVars
    {
        public static GraphicsDeviceManager graphics;
        public static RenderTarget2D preRenderTarget;
        public static SpriteBatch spriteBatch;

        /// <summary>The list of all fonts </summary>
        public static ListP<SpriteFont> fonts;

        /// <summary>The List of all sprites for the cursor.</summary>
        public static ListP<Texture2D> cursor;
        /// <summary>Determines whether or not the cursor is visible.</summary>
        public static Boolean showCursor;

        /// <summary>The List of all Menus in the game.</summary>
        public static ListP<Menu> menus;
        /// <summary>The List of all Levels in the game.</summary>
        public static ListP<Level> levels;
        /// <summary>The Menu-like interface available over the game level.</summary>
        public static Interface levelInterface;

        /// <summary>The List of all active Timers in the game. Inactive timers are removed automatically.</summary>
        public static List<Timer> timers;
        /// <summary>The List of all the Songs in the game.</summary>
        public static ListP<Song> music;
        /// <summary>The List of all the SoundEffects in the game.</summary>
        public static List<SoundEffect> effects;

        /// <summary>A large number of Boolean flags.</summary>
        public static Boolean[] flags;//flag 0 is isPlaying

        /// <summary>A GameObject outside of any specific levels, to be used for the player character.</summary>
        public static GameObject character;

        /// <summary>The current cursor location and button press information of the mouse.</summary>
        public static MouseState mouseState;
        /// <summary>The Point representation of the cursor location.</summary>
        public static Point mousePos;
        /// <summary>The Vector2 representation of the cursor location.</summary>
        public static Vector2 mouseLocation;

        /// <summary>The current button press information for the keyboard.</summary>
        public static KeyboardState keyboardState;

        /// <summary>A Rectangle representing the current location and size of the (2-dimensional) game camera.</summary>
        public static Rectangle camera;
    }
}
