﻿using System;
using System.Collections.Generic;

namespace IntuitiveRNG
{
    public static class IntuitiveRNG
    {
        public static double boostIncriment = 0.01;
        static List<int> Ids = new List<int>();
        static List<double> IdBoosts = new List<double>();
        static Random realRNG = new Random();

        public static double randomNumber
        {
            get
            {
                return realRNG.NextDouble();
            }
        }

        public static bool RandomChance(double probability, double? number = null, int Id = -1)
        {
            if (number == null)
            {
                number = randomNumber;
            }
            double newProbability = ExaggerateProbability(probability);
            if (Id != -1 && Ids.Contains(Id))
            {
                number -= IdBoosts[Ids.IndexOf(Id)];
            }
            if (number <= newProbability)//if thing occurred
            {
                if (Id != -1)
                {
                    if (Ids.Contains(Id))
                    {
                        if (IdBoosts[Ids.IndexOf(Id)] > 0)
                        {
                            IdBoosts[Ids.IndexOf(Id)] = 0;
                        }
                        IdBoosts[Ids.IndexOf(Id)] -= boostIncriment;
                    }
                    else
                    {
                        Ids.Add(Id);
                        IdBoosts.Add(boostIncriment);
                    }
                }
                return true;
            }
            else//if thing did not occur
            {
                if (Id != -1)
                {
                    if (Ids.Contains(Id))
                    {
                        if (IdBoosts[Ids.IndexOf(Id)] < 0)
                        {
                            IdBoosts[Ids.IndexOf(Id)] = 0;
                        }
                        IdBoosts[Ids.IndexOf(Id)] += boostIncriment;
                    }
                    else
                    {
                        Ids.Add(Id);
                        IdBoosts.Add(boostIncriment);
                    }
                }
                return false;
            }
        }

        /*public static Int32 RandomChance(Double[] probabilities, Double? number = null, Int32 Id = -1)
        {
            if (number == null)
            {
                number = randomNumber;
            }
            if (Id != -1 && Ids.Contains(Id))
            {
                number -= IdBoosts[Ids.IndexOf(Id)];
            }
            for (int i = 0; i < probabilities.Length; i++)
            {
                probabilities[i] = ExaggerateProbability(probabilities[i]);
            }

            if (number <= newProbability)
            {
                if (Id != -1)
                {
                    if (Ids.Contains(Id))
                    {
                        if (IdBoosts[Ids.IndexOf(Id)] > 0)
                        {
                            IdBoosts[Ids.IndexOf(Id)] = 0;
                        }
                        IdBoosts[Ids.IndexOf(Id)] -= boostIncriment;
                    }
                    else
                    {
                        Ids.Add(Id);
                        IdBoosts.Add(boostIncriment);
                    }
                }
                return true;
            }
            else
            {
                if (Id != -1)
                {
                    if (Ids.Contains(Id))
                    {
                        if (IdBoosts[Ids.IndexOf(Id)] < 0)
                        {
                            IdBoosts[Ids.IndexOf(Id)] = 0;
                        }
                        IdBoosts[Ids.IndexOf(Id)] += boostIncriment;
                    }
                    else
                    {
                        Ids.Add(Id);
                        IdBoosts.Add(boostIncriment);
                    }
                }
                return false;
            }
        }*/

        public static int GenerateID()
        {
            int id;
            do
            {
                id = realRNG.Next(int.MinValue, int.MaxValue);
            }
            while (Ids.Contains(id));
            return id;
        }

        public static double ExaggerateProbability(double probability, int iterations = 1, uint reductions = 0)
        {
            double oldProbability = probability;
            for (int i = 0; i < iterations; i++)
            {
                probability = 1 - Math.Cos(probability * Math.PI) * 0.5 - 0.5;
            }
            return (oldProbability * reductions + probability) / (reductions + 1.0);
        }
    }
}
