﻿using System;
using System.Collections.Generic;
using TernaryLogic;
using AnyBitVars;

namespace TernaryMath
{
    public class NTern
    {
        Tern[] bits;

        /// <summary>The decimal numeric value of the variable.</summary>
        public uint number
        {
            get
            {
                uint temp = 0;
                for (int i = 0; i < bits.Length; i++)
                {
                    temp += (uint)(bits[i].GetBalancedValue() + 1) * (uint)Math.Pow(3, i);
                }
                return temp;
            }

            set
            {
                long number = value;
                List<Tern> result = new List<Tern>();

                do
                {
                    result.Add(Tern.AllTernValues[number % Tern.AllTernValues.Length]);
                    number = number / Tern.AllTernValues.Length;
                }
                while (number > 0);

                bits = result.ToArray();
            }
        }

        public NTern (Tern[] bits)
        {
            this.bits = bits;
        }

        /// <summary>The number of ternary bits of information the variable can hold.</summary>
        public byte n
        {
            get
            {
                return (byte)bits.Length;
            }
        }

        public NTern()
        {
            if (this is Tern8)
            {
                bits = new Tern[8];
            }
            else if (this is Tern16)
            {
                bits = new Tern[16];
            }
            else if (this is Tern32)
            {
                bits = new Tern[32];
            }
            else if (this is Tern64)
            {
                bits = new Tern[64];
            }
            else
            {
                throw new Exception("The version of this constructor which takes zero arguments is only for use by specific child classes (Tern8, Tern16, Tern32, Tern64).");
            }
        }

        public NTern(byte n)
        {
            bits = new Tern[n];
        }

        /*public static explicit operator NTern(uint number)
        {
            
        }*/

        public static explicit operator uint(NTern var)
        {
            return var.number;
        }

        public static NTern operator ~(NTern tern)
        {
            for (int i = 0; i < tern.n; i++)
            {
                tern.bits[i] = !tern.bits[i];
            }
            return tern;
        }

        public static NTern operator &(NTern a, NTern b)
        {
            if (a.n == b.n)
            {
                NTern temp = new NTern(a.n);
                for (int i = 0; i < a.n; i++)
                {
                    temp.bits[i] = a.bits[i] && b.bits[i];
                }
                return temp;
            }
            else if (a.n > b.n)
            {
                NTern temp = new NTern(a.n);
                for (int i = 0; i < a.n; i++)
                {
                    if (i >= b.n)
                    {
                        temp.bits[i] = Tern.False;
                    }
                    else
                    {
                        temp.bits[i] = a.bits[i] && b.bits[i];
                    }
                }
                return temp;
            }
            else
            {
                NTern temp = new NTern(b.n);
                for (int i = 0; i < b.n; i++)
                {
                    if (i >= a.n)
                    {
                        temp.bits[i] = Tern.False;
                    }
                    else
                    {
                        temp.bits[i] = a.bits[i] && b.bits[i];
                    }
                }
                return temp;
            }
        }

        public static NTern operator |(NTern a, NTern b)
        {
            if (a.n == b.n)
            {
                NTern temp = new NTern(a.n);
                for (int i = 0; i < a.n; i++)
                {
                    temp.bits[i] = a.bits[i] || b.bits[i];
                }
                return temp;
            }
            else if (a.n > b.n)
            {
                NTern temp = new NTern(a.n);
                for (int i = 0; i < a.n; i++)
                {
                    if (i >= b.n)
                    {
                        temp.bits[i] = a.bits[i];
                    }
                    else
                    {
                        temp.bits[i] = a.bits[i] || b.bits[i];
                    }
                }
                return temp;
            }
            else
            {
                NTern temp = new NTern(b.n);
                for (int i = 0; i < b.n; i++)
                {
                    if (i >= a.n)
                    {
                        temp.bits[i] = b.bits[i];
                    }
                    else
                    {
                        temp.bits[i] = a.bits[i] || b.bits[i];
                    }
                }
                return temp;
            }
        }
        
        public class Tern8 : NTern { }

        public class Tern16 : NTern { }

        public class Tern32 : NTern { }

        public class Tern64 : NTern { }
    }
}
