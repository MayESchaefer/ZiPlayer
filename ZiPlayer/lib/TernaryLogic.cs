﻿using System;
using System.Collections.Generic;
using AnyBitVars;

namespace TernaryLogic
{
    public class Tern
    {
        bool? value = null;

        /// <summary>A ternary value representing true.</summary>
        public static readonly Tern True = new Tern(1);
        /// <summary>A ternary value representing false.</summary>
        public static readonly Tern False = new Tern(-1);
        /// <summary>A ternary value representing undefined, zero, or null.</summary>
        public static readonly Tern U = new Tern(0);
        /// <summary>An array containing all possible ternary values ordered lowest to highest.</summary>
        public static readonly Tern[] AllTernValues = new Tern[] { False, U, True };

        public Tern(bool value)
        {
            this.value = value;
        }

        /// <summary>
        /// Constructs a ternary variable based on a Balanced Ternary (-1, 0, or +1) signed integer.
        /// Values other than -1, 0, and 1 will not be accepted.
        /// </summary>
        /// <param name="value">The Balanced Ternary value.</param>
        public Tern(sbyte value)
        {
            if (value == -1)
            {
                this.value = false;
            }
            else if (value == 0)
            {
                this.value = null;
            }
            else if (value == 1)
            {
                this.value = true;
            }
            else
            {
                throw new ArgumentOutOfRangeException("The value must be -1, 0, or 1.");
            }
        }

        /// <summary>
        /// Constructs a ternary variable based on a Redundant Binary Representation of a rernary number.
        /// 00 (-1) is false, 01 and 10 (1/2) are undefined, and 11 (1) is true.
        /// </summary>
        /// <param name="value"></param>
        public Tern(Morsel value)
        {
            switch (value.RBR)
            {
                case -1f:
                    this.value = false;
                    break;
                case 1f:
                    this.value = true;
                    break;
                default:
                    this.value = null;
                    break;
            }
        }

        public sbyte GetBalancedValue()
        {
            if (!this)
            {
                return -1;
            }
            else if (this)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public static bool operator true(Tern a)
        {
            return a == True;
        }

        public static bool operator false(Tern a)
        {
            return a == False;
        }

        public static implicit operator Tern(bool? boolean)
        {
            if (boolean == true)
            {
                return True;
            }
            else if (boolean == false)
            {
                return False;
            }
            else
            {
                return U;
            }
        }

        public static explicit operator Tern(bool boolean)
        {
            if (boolean)
            {
                return True;
            }
            else
            {
                return False;
            }
        }

        public static implicit operator bool?(Tern tern)
        {
            if (tern)
            {
                return true;
            }
            else if (!tern)
            {
                return false;
            }
            else
            {
                return null;
            }
        }

        public static explicit operator bool(Tern tern)
        {
            if (tern)
            {
                return true;
            }
            else if (!tern)
            {
                return false;
            }
            else
            {
                return (bool)(bool?)tern;
            }
        }

        public static Tern operator !(Tern a)
        {
            if (a == U)
            {
                return U;
            }
            else if (a == False)
            {
                return True;
            }
            else
            {
                return False;
            }
        }

        public static Tern operator &(Tern a, Tern b)
        {
            if (a == False || b == False)
            {
                return False;
            }
            else if (a == U || b == U)
            {
                return U;
            }
            else
            {
                return True;
            }
        }

        public static Tern operator |(Tern a, Tern b)
        {
            if (a == True || b == True)
            {
                return True;
            }
            else if (a == U || b == U)
            {
                return U;
            }
            else
            {
                return False;
            }
        }

        public static Tern operator <(Tern a, Tern b)
        {
            if (a == b)//TT, UU, FF//equal
            {
                return False;
            }
            else if (!a || b)//FU, FT, UT//less
            {
                return True;
            }
            else//TU, TF, UF//greater
            {
                return False;
            }
        }

        public static Tern operator >(Tern a, Tern b)
        {
            return b < a;
        }

        public static Tern operator <=(Tern a, Tern b)
        {
            if (a == b)
            {
                return True;
            }
            else
            {
                return a < b;
            }
        }

        public static Tern operator >=(Tern a, Tern b)
        {
            return b <= a;
        }
    }
}
