﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using ZiPlayer.lib;
using static ZiPlayer.lib.Methods;
using static ZiPlayer.lib.GameVars;
using static ZiPlayer.Program;

namespace ZiPlayer.lib
{
    static class Methods
    {
        public static void ClickedOutsideButton()
        {

        }

        public static void ResetPlayerLocation(byte point)
        {
            character.bounds.Location = levels.GetValueAtPoint().playerLocation;
        }

        public static void ReplaySong()
        {
            PlaySong((byte)music.point);
        }

        public static void PlaySong(byte songId)
        {
            if (music.point != songId)
            {
                music.point = songId;
                return;
            }
            Timer temp = new Timer((float)music[songId].Duration.TotalSeconds, false);
            temp.OnTimerTick += ReplaySong;
            timers.Add(temp);
            MediaPlayer.Play(music.GetValueAtPoint());
        }

        public static void UpdateInputs()
        {
            keyboardState = Keyboard.GetState();
            mouseState = Mouse.GetState();

            mouseLocation.X = mouseState.X * ((float)camera.Width / Properties.Settings.Default.Dimensions.X);
            mouseLocation.Y = mouseState.Y * ((float)camera.Height / Properties.Settings.Default.Dimensions.Y);

            mousePos = new Point((int)Math.Round(mouseLocation.X), (int)Math.Round(mouseLocation.Y));
        }

        public static void UpdateTimers(GameTime gameTime)
        {
            for (int i = 0; i < timers.Count; i++)
            {
                if (!timers[i].active)
                {
                    continue;
                }
                timers[i].timeSinceTick += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timers[i].timeSinceTick >= timers[i].totalTimeTilTick - (float)gameTime.ElapsedGameTime.TotalSeconds)
                {
                    timers[i].TriggerEvent();
                    if (timers[i].repeat)
                    {
                        timers[i].timeSinceTick = 0f;

                    }
                    else
                    {
                        timers.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        public static void UpdateButtons(List<Button> buttons)
        {
            foreach (Button button in buttons)
            {
                if (button.hitbox.Contains(mousePos))
                {
                    if (mouseState.RightButton == ButtonState.Pressed)
                    {
                        button.onRightClick();
                    }
                    else if (mouseState.LeftButton == ButtonState.Pressed)
                    {
                        button.onLeftClick();
                    }
                    else if (mouseState.MiddleButton == ButtonState.Pressed)
                    {
                        button.onMiddleClick();
                    }
                    else
                    {
                        button.onHover();
                    }
                }
                else
                {
                    button.onNotHovering();

                    if (mouseState.RightButton == ButtonState.Pressed || mouseState.LeftButton == ButtonState.Pressed || mouseState.MiddleButton == ButtonState.Pressed)
                    {
                        ClickedOutsideButton();
                    }
                }
            }
        }
        
        public static void ProcessKeys()
        {
            foreach (KeyConfig config in Properties.Settings.Default.KeyConfig.configs)
            {
                if (config.IsCombo)
                {
                    bool isUp = false;
                    foreach (Keys key in config.Keys)
                    {
                        if (keyboardState.IsKeyUp(key))
                        {
                            isUp = true;
                        }
                    }
                    if (isUp)
                    {
                        config.KeyUp();
                    }
                    else
                    {
                        config.KeyDown();
                    }
                }
                else
                {
                    bool isDown = false;
                    foreach (Keys key in config.Keys)
                    {
                        if (keyboardState.IsKeyDown(key))
                        {
                            isDown = true;
                            break;
                        }
                    }
                    if (isDown)
                    {
                        config.KeyDown();
                    }
                    else
                    {
                        config.KeyUp();
                    }
                }
            }
        }
    }

    public static class Initializers
    {
        public static void InitializeVariables()
        {
            fonts = new ListP<SpriteFont>();
            cursor = new ListP<Texture2D>();
            showCursor = true;
            menus = new ListP<Menu>();
            levels = new ListP<Level>();
            levelInterface = new Interface();
            timers = new List<Timer>();
            music = new ListP<Song>();
            music.OnPointChanged += PlaySong;
            effects = new List<SoundEffect>();
            flags = new bool[65536];
            camera = new Rectangle(0, 0, Properties.Settings.Default.Dimensions.X / 2, Properties.Settings.Default.Dimensions.Y / 2);
        }
        
        public static void SetupSettingsAndGraphics()
        {
            if (Properties.Settings.Default.KeyConfig == null)
            {
                Properties.Settings.Default.KeyConfig = new KeyConfigSettings();
            }
            if (Properties.Settings.Default.Fullscreen && GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height == Properties.Settings.Default.Dimensions.X / (float)Properties.Settings.Default.Dimensions.Y)
            {
                graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                graphics.IsFullScreen = true;
            }
            else
            {
                graphics.PreferredBackBufferWidth = Properties.Settings.Default.Dimensions.X;
                graphics.PreferredBackBufferHeight = Properties.Settings.Default.Dimensions.Y;
            }
            graphics.ApplyChanges();
        }

        public static void StartMusic()
        {
            if (flags[0] && levels.GetValueAtPoint().songId >= 0)
            {
                Timer temp = new Timer((float)music[menus.GetValueAtPoint().songId].Duration.TotalSeconds, false);
                temp.OnTimerTick += ReplaySong;
                timers.Add(temp);
                PlaySong((byte)levels.GetValueAtPoint().songId);
            }
            else if (!flags[0] && menus.GetValueAtPoint().songId >= 0)
            {
                Timer temp = new Timer((float)music[menus.GetValueAtPoint().songId].Duration.TotalSeconds, false);
                temp.OnTimerTick += ReplaySong;
                timers.Add(temp);
                PlaySong((byte)menus.GetValueAtPoint().songId);
            }
        }

        /// <summary>
        /// Shakes the <see cref="camera"/> back and forth by the specified amount in the specified direction the specified number of times.
        /// </summary>
        /// <param name="direction">The direction in which to shake the camera.</param>
        /// <param name="amount">The amount by which to move the camera.</param>
        /// <param name="multiplier">The number of times to shake the camera. Non-integer values are supported.</param>
        public static void ShakeCamera (double direction, float amount, float multiplier)
        {
            int oddOrEven = 1;
            for (float i = 0f; i < multiplier; i += 0.5f)
            {
                if ((multiplier - i) < 0.5f)
                {
                    amount *= 0.5f / (multiplier - i);
                }
                MoveCamera(direction, amount * oddOrEven);
                oddOrEven *= -1;
            }
        }

        /// <summary>
        /// Moves the <see cref="camera"/> in the specified direcion by the specified amount.
        /// </summary>
        /// <param name="direction">The angle (in radians) at which to move the camera.</param>
        /// <param name="amount">The amount to move the camera.</param>
        public static void MoveCamera (double direction, float amount)
        {
            MoveCamera((int)Math.Round(amount * Math.Cos(direction)), (int)Math.Round(amount * Math.Sin(direction)));
        }

        /// <summary>
        /// Move the <see cref="camera"/> by the specified X and Y values.
        /// </summary>
        /// <param name="X">The amount by which the camera will be moved right.</param>
        /// <param name="Y">The amount by which the camera will be moved down.</param>
        public static void MoveCamera(int X, int Y)
        {
            camera.X += X;
            camera.Y += Y;
        }
    }

    public static class Overloads
    {
        /// <summary>
        /// Creates a <see cref="ListP{T}"/> from an <see cref="IEnumerable{T}"/>.
        /// </summary>
        public static ListP<T> ToListP<T> (this T[] values)
        {
            List<T> temp = values.ToList();
            ListP<T> temp2 = new ListP<T>();
            temp2.AddRange(temp);
            return temp2;
        }
    }
}
