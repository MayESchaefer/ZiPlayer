﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using AnyBitVars;

namespace ZiPlayer.lib
{
    /// <summary>A representation of a List, with a point representing the active entry.</summary>
    public class ListP<T> : List<T>
    {
        public delegate void EventHandler(byte point);
        public event EventHandler OnPointChanged;
        private int _point = 0;
        public int point
        {
            get { return _point; }
            set
            {
                _point = value;
                if (point >= 0)
                {
                    OnPointChanged?.Invoke((byte)_point);
                }
            }
        }

        public T GetValueAtPoint()
        {
            return this[point];
        }
    }

    /// <summary>Represents a countdown timer.</summary>
    public class Timer
    {
        public delegate void EventHandler();
        public double totalTimeTilTick, timeSinceTick = 0f;
        public bool repeat;
        public bool active = true;
        public event EventHandler OnTimerTick;

        public void TriggerEvent()
        {
            OnTimerTick?.Invoke();
        }

        public Timer(double totalTimeTilTick = 0f, bool repeat = false)
        {
            this.totalTimeTilTick = totalTimeTilTick;
            this.repeat = repeat;
        }
    }

    /// <summary>A representation of a keyPress or collection of synchronous keypresses, linked to methods run when the key(s) is pressed and/or released.</summary>
    public class KeyConfig
    {
        public List<Keys> Keys = new List<Keys>();
        public bool IsCombo;
        public delegate void EventHandler();
        public event EventHandler OnKeyDown, OnKeyUp;
        private bool IsDown;

        /// <summary>Creates a new <see cref="KeyConfig"/> with the specified keys.</summary>
        /// <param name="keys">The List of all keys to link to the method(s).</param>
        /// <param name="isCombo">Defines whether or not the keys all must be pressed in order for the method(s) to trigger, or if any of them (pressed individually) will trigger the event.</param>
        public KeyConfig(List<Keys> keys, bool isCombo = false)
        {
            Keys = keys;
            IsCombo = isCombo;
        }

        /// <summary>Creates a new <see cref="KeyConfig"/> with the specified keys.</summary>
        /// <param name="keys">The List of all keys to link to the method(s).</param>
        /// <param name="isCombo">Defines whether or not the keys all must be pressed in order for the method(s) to trigger, or if any of them (pressed individually) will trigger the event.</param>
        public KeyConfig(bool isCombo = false, params Keys[] keys)
        {
            Keys = keys.ToList();
            IsCombo = isCombo;
        }

        /// <summary>This method is automatically run when the program detects a specific key being pressed. You should never need to run this method manually.</summary>
        public void KeyDown()
        {
            OnKeyDown();
            IsDown = true;
        }

        /// <summary>This method is automatically run when the program does not detect a specific key being pressed. You should never need to run this method manually.</summary>
        public void KeyUp()
        {
            if (IsDown)
            {
                OnKeyUp();
                IsDown = false;
            }
        }
    }

    /// <summary>Represents a Serialized List of <see cref="KeyConfigs"/>, for storage in the program settings.</summary>
    [System.Configuration.SettingsSerializeAsAttribute(System.Configuration.SettingsSerializeAs.Binary)]
    public class KeyConfigSettings
    {
        private const System.Configuration.SettingsSerializeAs settingsSerializeAs = System.Configuration.SettingsSerializeAs.Binary;

        public List<KeyConfig> configs = new List<KeyConfig>();

        public KeyConfigSettings()
        {
            configs = new List<KeyConfig>();
        }

        public KeyConfigSettings(params KeyConfig[] configs)
        {
            this.configs = configs.ToList();
        }
    }

    /// <summary>Represents a clickable <see cref="Menu"/> button, with methods to be run when it is clicked.</summary>
    public class Button
    {
        public delegate void EventHandler();
        public event EventHandler RightClick, RightDown, LeftClick, LeftDown, MiddleClick, MiddleDown, Hover;

        /// <summary>The ListP of all sprites available to the button. sprites[0] is the default sprite; all others are extra.</summary>
        public ListP<Texture2D> sprites;
        public Polygon hitbox;
        public Rectangle bounds;
        bool rightDown = false, leftDown = false, middleDown = false;

        public Button() { }

        public Button(Rectangle bounds, params Texture2D[] sprites)
        {
            this.bounds = bounds;
            hitbox = (Polygon)bounds;
            this.sprites = sprites.ToListP();
        }

        public Button(Rectangle bounds, Polygon hitbox, params Texture2D[] sprites)
        {
            this.bounds = bounds;
            this.hitbox = hitbox;
            this.sprites = sprites.ToListP();
        }

        public void onRightClick()
        {
            RightDown?.Invoke();
            rightDown = true;
        }

        public void onLeftClick()
        {
            LeftDown?.Invoke();
            leftDown = true;
        }

        public void onMiddleClick()
        {
            MiddleDown?.Invoke();
            middleDown = true;
        }

        public void onHover()
        {
            if (rightDown)
            {
                RightClick?.Invoke();
                rightDown = false;
            }
            else if (leftDown)
            {
                LeftClick?.Invoke(); ;
                leftDown = false;
            }
            else if (middleDown)
            {
                MiddleClick?.Invoke();
                middleDown = false;
            }
            else
            {
                Hover?.Invoke();
            }
        }

        public void onNotHovering()
        {
            if (rightDown) rightDown = false;
            if (leftDown) leftDown = false;
            if (middleDown) middleDown = false;
        }
    }

    /// <summary>Represents copy with text location, message, and font.</summary>
    public class Text
    {
        public Vector2 location;
        public string message;
        public SpriteFont font;

        public Text() { }

        public Text(Vector2 location, string message, SpriteFont font)
        {
            this.location = location;
            this.message = message;
            this.font = font;
        }
    }

    /// <summary>Represents a menu with a series of <see cref = "Button"/>s and <see cref = "Text"/>, a background <see cref = "Color"/>, and a song to be played while the menu is open.</summary>
    public class Menu
    {
        public List<Button> buttons;
        public List<Text> copy;
        public Color backgroundColor;
        public int songId;//-1 is none

        public Menu(int songId = -1)
        {
            backgroundColor = Color.White;
            buttons = new List<Button>();
            copy = new List<Text>();
            this.songId = songId;
        }

        public Menu(Color backgroundColor, List<Button> buttons = null, List<Text> copy = null, int songId = -1)
        {
            this.backgroundColor = backgroundColor;
            this.copy = copy ?? new List<Text>();
            this.buttons = buttons ?? new List<Button>();
            this.songId = songId;
        }
    }

    /// <summary>Represents an animatable object (with a hitbox), to be placed within the game world.</summary>
    public class GameObject
    {
        public Point centerPoint;
        private double _rotation = 0;
        public double rotation
        {
            get
            {
                return _rotation;
            }
        }
        public Polygon hitbox;
        public Rectangle bounds;
        public ListP<Texture2D> sprites;
        public List<bool> flags;
        public Animation animation;
        public uint animationFrame = 0;
        public float animationTime = 0f;
        public bool isAnimating = false;

        //public GameObject() { }

        public GameObject(Rectangle bounds, ListP<Texture2D> sprites)
        {
            centerPoint = Point.Zero;
            this.bounds = bounds;
            hitbox = (Polygon)bounds;
            this.sprites = sprites;
        }

        public GameObject(Rectangle bounds, Point centerPoint, Polygon hitbox, ListP<Texture2D> sprites)
        {
            this.centerPoint = centerPoint;
            this.bounds = bounds;
            this.hitbox = hitbox;
            this.sprites = sprites;
        }

        public GameObject(Rectangle bounds, Rectangle hitbox, ListP<Texture2D> sprites)
        {
            centerPoint = Point.Zero;
            this.bounds = bounds;
            this.hitbox = (Polygon)hitbox;
            this.sprites = sprites;
        }

        public void RotateRel(double amount)
        {
            hitbox.Rotate(centerPoint, amount);
            _rotation = rotation + amount;
        }

        public void RotateAbs(double amount)
        {
            hitbox.Rotate(centerPoint, rotation * -1);
            hitbox.Rotate(centerPoint, amount);
            _rotation = amount;
        }

        public void Animate(Animation animation, byte startingFrame = 0)
        {
            this.animation = animation;
            animationFrame = startingFrame;
        }

        public double Collides(GameObject obj)
        {
            if (hitbox.Intersects(obj.hitbox))
            {
                return Math.Atan2(obj.centerPoint.Y - centerPoint.Y, obj.centerPoint.X - centerPoint.X);
            }
            else
            {
                return -1;
            }
        }
    }

    /// <summary>Represents an animation, to be applied frame by frame to a <see cref = "GameObject"/>.</summary>
    public class Animation
    {
        public ListP<float> delays = new ListP<float>();
        public ListP<byte> frames = new ListP<byte>();
        public bool loopIndefinitely;

        public Animation(ListP<float> delays, ListP<byte> frames, bool loopIndefinitely = false)
        {
            if (delays.Count == frames.Count)
            {
                this.delays = delays;
                this.frames = frames;
                this.loopIndefinitely = loopIndefinitely;
            }
            else
            {
                throw new ArgumentException("Argument lengths must be identical.");
            }
        }
    }

    /// <summary>Represents an interface to be displayed over the game world with <see cref = "Button"/>s, <see cref = "Text"/>, and <see cref = "GameObject"/>s.</summary>
    public class Interface
    {
        public List<Button> buttons;
        public List<Text> copy;
        public List<GameObject> objects;

        public Interface(List<Text> copy = null, List<Button> buttons = null, List<GameObject> objects = null)
        {
            if (copy == null)
            {
                this.copy = new List<Text>();
            }
            else
            {
                this.copy = copy;
            }
            if (buttons == null)
            {
                this.buttons = new List<Button>();
            }
            else
            {
                this.buttons = buttons;
            }
            if (objects == null)
            {
                this.objects = new List<GameObject>();
            }
            else
            {
                this.objects = objects;
            }
        }
    }

    /// <summary>Represents a game level with a starting location for the player, a list of <see cref = "GameObject"/>s, and background music.</summary>
    public class Level
    {
        public List<GameObject> objects;
        public Point playerLocation;
        public int songId;//-1 is none

        public Level(Point playerLocation, int songId = -1, params GameObject[] objects)
        {
            this.playerLocation = playerLocation;
            this.songId = songId;
            this.objects = objects.ToList();
        }

        public Level(Point playerLocation, int songId = -1, List<GameObject> objects = null)
        {
            this.playerLocation = playerLocation;
            this.songId = songId;
            this.objects = objects ?? new List<GameObject>();
        }
    }

}
