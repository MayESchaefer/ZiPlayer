﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AnyBitVars
{
    public class NBitVar
    {
        private bool[] Bits;
        public bool[] bits
        {
            get
            {
                return Bits;
            }
        }

        /// <summary>The decimal numeric value of the variable.</summary>
        public uint number
        {
            get
            {
                uint temp = 0;
                bool[] tempBits = Bits;
                Array.Reverse(tempBits);
                for (int i = 0; i < Bits.Length; i++)
                {
                    if (tempBits[i])
                    {
                        temp += (uint)Math.Pow(2, i);
                    }
                }
                return temp;
            }

            set
            {
                byte test = (byte)Convert.ToString(value, 2).Length;
                if (Bits.Length < (byte)Convert.ToString(value, 2).Length)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                else
                {
                    Bits = Convert.ToString(value, 2).PadLeft(Bits.Length, '0').Select(s => s.Equals('1')).ToArray();
                }
            }
        }

        /// <summary>The number of bits of information the variable can hold.</summary>
        public byte n
        {
            get
            {
                return (byte)Bits.Length;
            }
        }

        /// <summary>Constructs an NBitVar with the specified number of bits of storage.</summary>
        /// <param name="n">The number of bits of information the variable can hold.</param>
        public NBitVar(byte n)
        {
            Bits = new bool[n];
        }

        /// <summary>Constructs an NBitVar with the specified value, and the minimum amount of storage required to store that value.</summary>
        /// <param name="value">The value which the variable will contain.</param>
        public NBitVar(uint value)
        {
            Bits = new bool[(byte)Math.Ceiling(Math.Log(value)) + 1];
            number = value;
        }

        /// <summary>Constructs an NBitVar with the specified value and amount of storage.</summary>
        /// <param name="n">The number of bits of information the variable can hold.</param>
        /// <param name="value">The value which the variable will contain.</param>
        public NBitVar(byte n, uint value)
        {
            Bits = new bool[n];
            number = value;
        }

        /// <summary>The signed numeric value of the variable.</summary>
        public int signedValue
        {
            get
            {
                if (Bits.Length == 1)
                {
                    return Bits[0] ? 1 : -1;
                }
                else
                {
                    if (Bits[0])
                    {
                        return ((int)((number - 1) << 1)) * -1;
                    }
                    else
                    {
                        return (int)(number << 1);
                    }
                }
            }

            set
            {
                if (value < 0)
                {
                    number = (uint)value >> 1;
                }
                else
                {
                    number = 1 + ((uint)value >> 1);
                }
            }
        }

        /// <summary>Gets the value of the variable, created from the specified set of values.</summary>
        /// <param name="set">The set of all characters which make up the returned value.</param>
        /// <returns></returns>
        public string GetSetValue(string[] set)
        {
            long value = number;
            value = number;
            string result = string.Empty;
            while (true)
            {
                result = set[value % set.Length] + result;
                value = value / set.Length;
                if (value <= 0)
                {
                    break;
                }
            }

            return result;
        }

        public static explicit operator uint(NBitVar var)
        {
            return var.number;
        }
    }

    /// <summary>A two bit variable.</summary>
    public class Morsel : NBitVar
    {
        public Morsel() : base(2) { }

        public float RBR
        {
            get
            {
                if (bits[0] != bits[1])
                {
                    return 1f / 2f;
                }
                else if (bits[0] == false)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }

            set
            {
                if (value == 1f)
                {
                    number = 3;
                }
                else if (value == -1f)
                {
                    number = 0;
                }
                else
                {
                    Random rand = new Random();
                    if (Convert.ToBoolean(rand.Next(0, 1)))
                    {
                        number = 1;
                    }
                    else
                    {
                        number = 2;
                    }
                }
            }
        }
    }

    /// <summary>A four bit variable.</summary>
    public class Nibble : NBitVar
    {
        public Nibble() : base(4) { }
    }
}