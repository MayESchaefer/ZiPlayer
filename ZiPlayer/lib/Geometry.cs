﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace ZiPlayer.lib
{
    public class LineSegment
    {
        public Point a, b;

        public LineSegment(Point a, Point b)
        {
            this.a = a;
            this.b = b;
        }
        
        private static bool PointIsOnLine(LineSegment a, Point b)
        {
            LineSegment aTmp = new LineSegment(new Point(0, 0), new Point(a.b.X - a.a.X, a.b.Y - a.a.Y));
            Point bTmp = new Point(b.X - a.a.X, b.Y - a.a.Y);
            double r = aTmp.b.X * bTmp.Y - bTmp.X * aTmp.b.Y;
            return Math.Abs(r) < 0.000001;
        }

        private static bool PointIsRightOfLine(LineSegment a, Point b)
        {
            LineSegment aTmp = new LineSegment(new Point(0, 0), new Point(a.b.X - a.a.X, a.b.Y - a.a.Y));
            Point bTmp = new Point(b.X - a.a.X, b.Y - a.a.Y);
            return (aTmp.b.X * bTmp.Y - bTmp.X * aTmp.b.Y) < 0;
        }

        private static bool LineSegmentTouchesOrCrossesLine(LineSegment a, LineSegment b)
        {
            return PointIsOnLine(a, b.a) || PointIsOnLine(a, b.b) || (PointIsRightOfLine(a, b.a) ^ PointIsRightOfLine(a, b.b));
        }

        public bool Intersects(LineSegment line)
        {
            return new Rectangle(a.X, a.Y, b.X - a.X, b.Y - a.Y).Intersects(new Rectangle(line.a.X, line.a.Y, line.b.X - line.a.X, line.b.Y - line.a.Y))
                    && LineSegmentTouchesOrCrossesLine(this, line)
                    && LineSegmentTouchesOrCrossesLine(line, this);
        }
    }

    public class Polygon
    {
        List<Point> points;

        public Polygon(params Point[] points)
        {
            this.points = points.ToList();
        }

        public Polygon(List<Point> points)
        {
            this.points = points;
        }

        public bool Contains(Point p)
        {
            int sides = points.Count;
            int j = sides - 1;
            bool pointStatus = false;
            for (int i = 0; i < sides; i++)
            {
                if (points[i].Y < p.Y && points[j].Y >= p.Y || points[j].Y < p.Y && points[i].Y >= p.Y)
                {
                    if (points[i].X + (p.Y - points[i].Y) / (points[j].Y - points[i].Y) * (points[j].X - points[i].X) < p.X)
                    {
                        pointStatus = !pointStatus;
                    }
                }
                j = i;
            }
            return pointStatus;
        }

        public bool Intersects(Polygon poly)
        {
            for (int j = 0; j < poly.points.Count; j++)
            {
                if (Contains(poly.points[j]))
                {
                    return true;
                }
                for (int i = 0; i < points.Count; i++)
                {
                    int tempI = i + 1, tempJ = j + 1;
                    if (i == points.Count - 1)
                    {
                        tempI = 0;
                    }
                    if (j == poly.points.Count - 1)
                    {
                        tempJ = 0;
                    }
                    if (new LineSegment(points[i], points[tempI]).Intersects(new LineSegment(poly.points[j], poly.points[tempJ])))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void Rotate(Point centerPoint, double amount)
        {
            for (int i = 0; i < points.Count; i++)
            {
                points[i] = RotatePoint(centerPoint, points[i], amount);
            }
        }

        private Point RotatePoint(Point center, Point rotatable, double rotateBy)
        {
            return new Point((int)((rotatable.X - center.X) * Math.Cos(rotateBy) - (rotatable.Y - center.Y) * Math.Sin(rotateBy)) + center.X, (int)((rotatable.X - center.X) * Math.Sin(rotateBy) + (rotatable.Y - center.Y) * Math.Cos(rotateBy)) + center.Y);
        }

        public static explicit operator Polygon(Rectangle rect)
        {
            return new Polygon(new Point(rect.Left, rect.Top), new Point(rect.Right, rect.Top), new Point(rect.Right, rect.Bottom), new Point(rect.Left, rect.Bottom));
        }
    }
}
