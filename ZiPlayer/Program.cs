﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Runtime.Remoting.Channels.Ipc;
using System.Security.Permissions;

namespace ZiPlayer
{
#if WINDOWS || LINUX
    /// <summary>The main class.</summary>
    public static class Program
    {
        public static string[] args;
        public static CurrentGame game;
        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        static void Main(string[] args)
        {
            System.Diagnostics.Process[] instances = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));
            if (instances.Length > 1)
            {
                try
                {
                    for (int i = 0; i < instances.Length; i++)
                    {
                        if (instances[i].Id != System.Diagnostics.Process.GetCurrentProcess().Id)
                        {
                            instances[i].Kill();
                        }
                    }
                }
                catch
                {
                    for (int i = 0; i < instances.Length; i++)
                    {
                        if (instances[i].Id == System.Diagnostics.Process.GetCurrentProcess().Id)
                        {
                            instances[i].Kill();
                        }
                    }
                }
            }
            Program.args = args;
            game = new CurrentGame();
            game.Run();
        }
    }
#endif
}
